#pragma once

#include <CEGUI.h>
#include "MantisGUI.h"


/**	
 *	CEGUI Window for the Pause Menu.
 */
class PauseMenu
{
private:

	//Pointer to the PauseMenu root window
	CEGUI::Window*	m_PauseMenuWindow;

	//Previx given to layout
	CEGUI::String	m_NamePrefix;

	//Identifier
	static int		m_InstanceNumber;

	//Display bool
	bool m_Display;

	MantisGUI* m_Owner;


public:
	PauseMenu(MantisGUI* owner);

	~PauseMenu(void);

	void Init(void);

	void setVisible(bool visible);

	bool isVisible(void);

	void ToggleVisible();

private:

	void CreateCEGUIWindow(void);

	//Event handling
	void RegisterHandlers(void);

	bool Handle_ResumeGameButtonPressed(const CEGUI::EventArgs& e);

	bool Handle_OptionsButtonPressed(const CEGUI::EventArgs& e);

	bool Handle_QuitMenuButtonPressed(const CEGUI::EventArgs& e);

	bool Handle_QuitDesktopButtonPressed(const CEGUI::EventArgs& e);

	bool Handle_ButtonHovered(const CEGUI::EventArgs& e);

	void PlayClick(void);

};