#include "Console.h"
#include "ConsoleTextBox.h"
#include "ConsoleDelegate.h"
#include <CEGUI.h>
#include "../../FRAMEWORK/Utility/Camera.h"

int Console::m_InstanceNumber;

Console::Console(void)
{
	m_ConsoleWindow = NULL;
	m_InstanceNumber = 0;
	m_NamePrefix = "";
	m_Console = false;
}

Console::~Console(void)
{
}

void Console::Init(void)
{
	CreateCEGUIWindow();
	setVisible(false);
}

void Console::setVisible(bool visible)
{
	m_ConsoleWindow->setVisible(visible);
	m_Console = visible;

	if (m_Console)
		m_ConsoleWindow->activate();

	else
		m_ConsoleWindow->deactivate();
}

void Console::toggleVisible(void)
{
	//If console isn't initialized bail out
	if (!m_ConsoleWindow)
		return;

	if (m_Console)
		setVisible(false);

	else
		setVisible(true);
}

bool Console::isVisible(void)
{
	return m_Console;
}

void Console::CreateCEGUIWindow(void)
{
	//Get a local pointer to the CEGUI window manager
	CEGUI::WindowManager* pWindowManager = CEGUI::WindowManager::getSingletonPtr();

	//Set and increase instance number
	m_NamePrefix = ++m_InstanceNumber + "_";

	//Create the console window and assign it to window manager
	m_ConsoleWindow = pWindowManager->loadWindowLayout("ConsoleLayout.layout", m_NamePrefix);

	if (m_ConsoleWindow)
	{
		//If window succeeds, attach it
		CEGUI::System::getSingleton().getGUISheet()->addChildWindow(m_ConsoleWindow);

		//Register the handlers for events
		(this)->RegisterHandlers();
	}
	else
	{
		//Log a failed event
		CEGUI::Logger::getSingleton().logEvent("Error: Unable to laod the console window from .layout");
	}
}

void Console::RegisterHandlers(void)
{
	//Register "send" button
	m_ConsoleWindow->getChild(m_NamePrefix + "ConsoleRoot/SendButton")->subscribeEvent(
		CEGUI::PushButton::EventClicked,
		CEGUI::Event::Subscriber(
		&Console::Handle_SendButtonPressed, this));

	//Register text submitted
	m_ConsoleWindow->getChild(m_NamePrefix + "ConsoleRoot/EditBox")->
		subscribeEvent(CEGUI::Editbox::EventTextAccepted,
		CEGUI::Event::Subscriber(&Console::Handle_TextSubmitted, this));
}

bool Console::Handle_TextSubmitted(const CEGUI::EventArgs& e)
{
	const CEGUI::WindowEventArgs* args = static_cast<const CEGUI::WindowEventArgs*>(&e);

	//Get the text from the editbox
	CEGUI::String Msg = m_ConsoleWindow->getChild(m_NamePrefix + "ConsoleRoot/EditBox")->getText();

	//Parse the message
	(this)->ParseText(Msg);

	//Clear the edit box 
	m_ConsoleWindow->getChild(m_NamePrefix + "ConsoleRoot/EditBox")->setText("");

	return true;
}

bool Console::Handle_SendButtonPressed(const CEGUI::EventArgs& e)
{
	//Get the text from the editbox
	CEGUI::String Msg = m_ConsoleWindow->getChild(m_NamePrefix + "ConsoleRoot/EditBox")->getText();

	//Parse the message
	(this)->ParseText(Msg);

	//Clear the edit box 
	m_ConsoleWindow->getChild(m_NamePrefix + "ConsoleRoot/EditBox")->setText("");

	return true;
}

void Console::ParseText(CEGUI::String inMsg)
{
	std::string inString = inMsg.c_str();

	if (inString.length() >= 1)
	{
		if (inString.at(0) == '/') //The first letter is a command
		{
			std::string::size_type commandEnd = inString.find(" ", 1);
			std::string command = inString.substr(1, commandEnd - 1);
			std::string commandArgs = inString.substr(commandEnd + 1, inString.length() - (commandEnd + 1));

			//Convert command to lower case
			for (std::string::size_type i = 0; i < command.length(); i++)
			{
				command[i] = tolower(command[i]);
			}

			//Process
			if (command == "say")
			{
				std::string outString = "You: " + commandArgs; //Append name to the message
				OutputText(outString);
			}
			else if (command == "quit")
			{
			}
			else if (command == "help")
			{
				std::string outString = "/say	Say out loud";
				OutputText(outString);
				outString = "/help	Display help";
				OutputText(outString);
				outString = "/reloadai <filename> load ai from file";
				OutputText(outString);
				outString = "/saveai <filename> save ai to file";
				OutputText(outString);
				outString = "/camera <command> mode_FREEFORM, mode_FIXED";
				OutputText(outString);
			}
			else if (command == "reloadai")
			{
				CDI->ReloadAI(commandArgs);
				std::string outString = "AICore has reloaded from " + commandArgs;
				OutputText(outString);
			}
			else if (command == "saveai")
			{
				CDI->ParseAI(commandArgs);
				std::string outString = "AICore has saved to " + commandArgs;
				OutputText(outString);
			}
			else if (command == "camera")
			{
				std::string outString;

				if(commandArgs == "mode_FREEFORM")
				{
					outString = "Camera Mode Set to Freeform";
					gCamera->setMode(FREEFORM);
				}

				if(commandArgs == "mode_FIXED")
				{
					outString = "Camera Mode Set to Fixed Rotation.";
					gCamera->setMode(FIXED_ROTATION);
				}
		
				OutputText(outString);
			}
			else
			{
				std::string outString = "<" + inString + "> is an invalid command.";
				(this)->OutputText(outString, CEGUI::colour(1.0f, 0.0f, 0.0f));
			}
		}

		else
		{
			(this)->OutputText(inString); //Output without command
		}
	}
}

void Console::OutputText(CEGUI::String inMsg, CEGUI::colour colour)
{
	//Get a pointer to the ChatBox
	CEGUI::Listbox* outputWindow = static_cast<CEGUI::Listbox*>(m_ConsoleWindow->
		getChild(m_NamePrefix + "ConsoleRoot/ChatBox"));

	CEGUI::ConsoleTextBox* newItem = 0;
	newItem = new CEGUI::ConsoleTextBox(inMsg, CEGUI::HTF_WORDWRAP_LEFT_ALIGNED);

	newItem->setTextColours(colour);
	outputWindow->addItem(newItem);
}