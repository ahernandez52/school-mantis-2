#pragma once

#include <CEGUI.h>
#include "../../FRAMEWORK/Utility/MantisUtil.h"

class Actor;
class Player;
class MantisGUI;


/**	
 *	Struct used to display and update the "Targetable" units that have secondary reticles while the player is not targeting them.
 */
struct TargetableUnit
{
	Actor* actor;
	CEGUI::Window* window;

	UINT listID;
	CEGUI::ListboxTextItem* targetInfoName;
	CEGUI::ListboxTextItem* targetInfoDistance;
};


/**	
 *	CEGUI Window and logic for the Player Heads Up Display.
 */
class HUD
{
private:

	//Pointer to the HUD root window
	CEGUI::Window*	m_HUDWindow;

	//Previx given to layout
	CEGUI::String	m_NamePrefix;

	//Identifier
	static int		m_InstanceNumber;

	//Targetables identifier
	static int		m_TargetableNumber;

	//Display bool
	bool m_Display;

	//display player reticle
	bool m_PlayerReticle;

	MantisGUI* m_Owner;

	Player* m_Player;

	Actor* m_CurrentTarget;

	std::vector<TargetableUnit*> m_TargetableUnits;

public:
	HUD(MantisGUI* owner);

	~HUD(void);

	/**	
	 *	Updates the Time Until Next Wave display with specified num.
	 */
	void UpdateWaveMarker(float timeTillNextWave);

	/**	
	 *	Sets the current wave number display;
	 */
	void SetWaveNumber(int num);

	/**	
	 *	Updates the score display.
	 */
	void UpdateScore(int score);

	/**	\brief Updates the HUD.
	 *	
	 *	The Update method takes care of updating the player's current health and shield, the current player target,
	 *	any possible targetable units that exist, and the Target Info box.
	 */
	void Update(float dt);

	void Init(void);

	void setVisible(bool visible);

	bool isVisible(void);

	void ToggleVisible();

	void SetReticleVisible(bool visible);

	void ToggleReticle();

	void ClearHUD();

	void SetPlayer(Player* player);

	/**	
	 *	Selects the target closest to the current mouse position.
	 */
	void SelectTarget();

	/**	
	 *	Removes the current target from the HUD and gives them back their targetable reticle if they still exist.
	 */
	void RemoveTarget();

	/**	\brief Clamps the player target reticle to the screen.
	 *	
	 *	Keeps the target reticule on the screen.
	 *	If the reticle goes off the screen, this method will clamp it to one of the sides of the screen based on its position in the world.
	 */
	D3DXVECTOR2 ClampTargetReticle(D3DXVECTOR3 targetPosition);

private:

	void CreateCEGUIWindow(void);

	//Event handling
	void RegisterHandlers(void);

	bool Handle_MenuButtonPressed(const CEGUI::EventArgs& e);

	bool Handle_MenuHover(const CEGUI::EventArgs& e);

	bool Handle_MenuUnHover(const CEGUI::EventArgs& e);

	/**	
	 *	Helper function for Updating the current target reticle.
	 */
	void UpdateTargetReticle();

	/**	
	 *	Creates a window of any "targetable" unit on the field within a range.
	 */
	void CreateTargetableUnits();

	/**	
	 *	Updates the list of "targetable" units.
	 */
	void UpdateTargetableUnits();

	/**	
	 *	Removes and releases all the "targetable" units from the list of "targetable" units.
	 */
	void RemoveTargetableUnits();

	/**	
	 *	Depopulates and then repopulates the Target Info Log.
	 */
	void RePopulateTargetLog();

	/**	\brief Clamps all "targetable" unit reticles to the screen.
	 *	
	 *	Keeps the "targetable" unit reticules on the screen.
	 *	If a reticle goes off the screen, this method will clamp it to one of the sides of the screen based on its position in the world.
	 *	Works the same as HUD::ClampTargetReticle(Vec3 pos);
	 */
	D3DXVECTOR2 ClampSecondaryReticle(TargetableUnit* unit);
};

