/**
 * ConsoleDelegate - This object provides access to necessary console functions
 */

#pragma once

#include <iostream>

class Console;
class MantisGUI;
class AICore;

#define CDI ConsoleDelegate::Instance()

class ConsoleDelegate
{
private:

	//Pointer to the console
	Console*	m_Console;

	//Pointer to the AICore
	AICore*		m_AICore;

public:
	static ConsoleDelegate* Instance(void);

	void OutputText(const std::string& input);

private:

	ConsoleDelegate(void);
	ConsoleDelegate(const ConsoleDelegate&);
	ConsoleDelegate operator=(const ConsoleDelegate&);

	bool LinkConsole(Console* console);
	bool LinkAICore(AICore* core);

	//Save/Load the AI variables
	void ParseAI(std::string fileName);
	void ReloadAI(std::string fileName);

	friend class MantisGUI;
	friend class Console;
};