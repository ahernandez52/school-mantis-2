/**
 * ConsoleTextBox - A formatted text box which implements word wrapping
 *
 */

#pragma once

#include <CEGUI.h>

namespace CEGUI
{

class ConsoleTextBox : public ListboxTextItem
{
protected:
	//Current formatting set
	HorizontalTextFormatting m_formatting;

	//CeGui rendering string
	mutable FormattedRenderedString* m_formattedRenderedString;

	//Target rect
	mutable Size m_formattedAreaSize;

public:
	
	ConsoleTextBox(const String& text,
		const HorizontalTextFormatting format = HTF_LEFT_ALIGNED,
		const uint item_id = 0,
		void* const item_data = 0,
		const bool disabled = false,
		const bool auto_delete = true);

	~ConsoleTextBox(void);

	//Get formatting
	HorizontalTextFormatting getFormatting(void) const;

	//Set formatting
	void setFormatting(const HorizontalTextFormatting fmt);

	Size getPixelSize(void) const;
	void draw(GeometryBuffer& buffer, const Rect& targetRect,
		float alpha, const Rect* clipper) const;

protected:

	//Create a formatted rendered string
	void setupStringFormatter(void) const;

};

}