/**
 * MantisGUI - A wrapper class which is responsible for managing the CEGUI
 *
 * Author - Jesse Dillon
 */

#pragma once

#include <CEGUI.h>
#include "..\..\FRAMEWORK\Utility\MantisUtil.h"
#include <RendererModules\Direct3D9\CEGUIDirect3D9Renderer.h>
#include "../../FRAMEWORK/GameFramework.h"

class TestBed;
class Console;
class QuestDialog;
class OptionsMenu;
class MainMenu;
class PauseMenu;
class HUD;
class DeathSplash;

class MantisGUI
{
	//Pointer to the owner
	GameFramework*				m_GameFramework;

	//Renderer
	CEGUI::Direct3D9Renderer*	m_pRenderer;

	//System
	CEGUI::System*				m_pSysGUI;

	//Testbed menu
	//TestBed*			m_Testbed;

	//Console menu
	Console*			m_Console;

public:

	//main menu
	MainMenu*			m_MainMenu;

	//options menu
	OptionsMenu*		m_Options;

	//pause menu
	PauseMenu*			m_PauseMenu;

	//HUD
	HUD*				m_HUD;

	//Death splash screen
	DeathSplash*		m_DeathSplash;

public:
	//CTOR
	MantisGUI(GameFramework* game);

	~MantisGUI(void);

	//Initialize
	void Init(void);

	//Update
	void Update(float dt);

	//Draw
	void Draw(void);

	//Get system
	CEGUI::System* GetGui(void);

	CEGUI::Direct3D9Renderer* GetRenderer(void);

	//On Reset Device
	void OnResetDevice(void);

	//On Lost Device
	void OnLostDevice(void);

	//Get framework
	GameFramework* GetFramework(void){return m_GameFramework;}

	//Get graphics
	D3D9GraphicsCore* GetGraphics(void){return m_GameFramework->GetGraphics();}
	AICore*	GetAI(void){return m_GameFramework->GetAI();}
};