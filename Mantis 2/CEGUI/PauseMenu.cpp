#include "PauseMenu.h"
#include "../../GAMEPLAY/FinalDemoState.h"
#include "../../FRAMEWORK/Sound/Audio.h"

int PauseMenu::m_InstanceNumber = 0;

PauseMenu::PauseMenu(MantisGUI* owner) : m_Owner(owner)
{
	m_PauseMenuWindow = NULL;
	m_NamePrefix = "";
	m_Display = false;
}

PauseMenu::~PauseMenu(void)
{
	CEGUI::System::getSingleton().getGUISheet()->removeChildWindow(m_PauseMenuWindow);
	m_PauseMenuWindow->destroy();
}

//========================================
//  Private Functions
//========================================

void PauseMenu::Init(void)
{
	CreateCEGUIWindow();
	setVisible(true);
}

void PauseMenu::setVisible(bool visible)
{
	m_Display = visible;

	if(m_Display)
	{
		SCI->PlaySample("Assets/Sounds/Effects/GUI/Expands/hud_expand.wav");
		m_PauseMenuWindow->activate();
		m_PauseMenuWindow->show();
	}
	else
	{
		SCI->PlaySample("Assets/Sounds/Effects/GUI/Expands/hud_contract.wav");
		m_PauseMenuWindow->deactivate();
		m_PauseMenuWindow->hide();
	}
}

bool PauseMenu::isVisible(void)
{
	return m_Display;
}

void PauseMenu::ToggleVisible()
{
	if(isVisible())
	{
		setVisible(false);
	}
	else
	{
		setVisible(true);
	}
}

//========================================
//  Private Functions
//========================================

void PauseMenu::CreateCEGUIWindow(void)
{
	//Get a local pointer to the window manager
	CEGUI::WindowManager* pWindowManager = CEGUI::WindowManager::getSingletonPtr();

	//Set and increase instance id
	m_NamePrefix = ++m_InstanceNumber + "_";

	//Create the PauseMenu window and assign it to window manager
	m_PauseMenuWindow = pWindowManager->loadWindowLayout("PauseMenu.layout", m_NamePrefix);

	if (m_PauseMenuWindow)
	{
		//If window succeeds attach it
		CEGUI::System::getSingleton().getGUISheet()->addChildWindow(m_PauseMenuWindow);

		//Register handlers for events
		(this)->RegisterHandlers();
	}

	else
	{
		//Log a failed event
		CEGUI::Logger::getSingleton().logEvent("Error: Unable to load the PauseMenu window from .layout");
	}
}

void PauseMenu::RegisterHandlers(void)
{
	m_PauseMenuWindow->getChild(m_NamePrefix + "PauseMenuRoot/ResumeGameButton")->subscribeEvent(
		CEGUI::PushButton::EventClicked,
		CEGUI::Event::Subscriber(
		&PauseMenu::Handle_ResumeGameButtonPressed, this));

	m_PauseMenuWindow->getChild(m_NamePrefix + "PauseMenuRoot/OptionsButton")->subscribeEvent(
		CEGUI::PushButton::EventClicked,
		CEGUI::Event::Subscriber(
		&PauseMenu::Handle_OptionsButtonPressed, this));

	m_PauseMenuWindow->getChild(m_NamePrefix + "PauseMenuRoot/QuitMenuButton")->subscribeEvent(
		CEGUI::PushButton::EventClicked,
		CEGUI::Event::Subscriber(
		&PauseMenu::Handle_QuitMenuButtonPressed, this));

	m_PauseMenuWindow->getChild(m_NamePrefix + "PauseMenuRoot/QuitDesktopButton")->subscribeEvent(
		CEGUI::PushButton::EventClicked,
		CEGUI::Event::Subscriber(
		&PauseMenu::Handle_QuitDesktopButtonPressed, this));

	m_PauseMenuWindow->getChild(m_NamePrefix + "PauseMenuRoot/ResumeGameButton")->subscribeEvent(
		CEGUI::PushButton::EventMouseEnters,
		CEGUI::Event::Subscriber(
		&PauseMenu::Handle_ButtonHovered, this));

	m_PauseMenuWindow->getChild(m_NamePrefix + "PauseMenuRoot/OptionsButton")->subscribeEvent(
		CEGUI::PushButton::EventMouseEnters,
		CEGUI::Event::Subscriber(
		&PauseMenu::Handle_ButtonHovered, this));

	m_PauseMenuWindow->getChild(m_NamePrefix + "PauseMenuRoot/QuitMenuButton")->subscribeEvent(
		CEGUI::PushButton::EventMouseEnters,
		CEGUI::Event::Subscriber(
		&PauseMenu::Handle_ButtonHovered, this));

	m_PauseMenuWindow->getChild(m_NamePrefix + "PauseMenuRoot/QuitDesktopButton")->subscribeEvent(
		CEGUI::PushButton::EventMouseEnters,
		CEGUI::Event::Subscriber(
		&PauseMenu::Handle_ButtonHovered, this));
}

bool PauseMenu::Handle_ResumeGameButtonPressed(const CEGUI::EventArgs& e)
{
	PlayClick();
	static_cast<FinalDemoState*>(m_Owner->GetFramework()->getGSM()->GetCurrentState())->ResumeGame();
	
	return true;
}

bool PauseMenu::Handle_OptionsButtonPressed(const CEGUI::EventArgs& e)
{
	PlayClick();
	static_cast<FinalDemoState*>(m_Owner->GetFramework()->getGSM()->GetCurrentState())->Options();

	return true;
}

bool PauseMenu::Handle_QuitMenuButtonPressed(const CEGUI::EventArgs& e)
{
	PlayClick();
	static_cast<FinalDemoState*>(m_Owner->GetFramework()->getGSM()->GetCurrentState())->QuitMenu();

	return true;
}

bool PauseMenu::Handle_QuitDesktopButtonPressed(const CEGUI::EventArgs& e)
{
	PlayClick();
	static_cast<FinalDemoState*>(m_Owner->GetFramework()->getGSM()->GetCurrentState())->QuitDesktop();

	return true;
}

bool PauseMenu::Handle_ButtonHovered(const CEGUI::EventArgs& e)
{
	SCI->PlaySample("Assets/Sounds/Effects/GUI/Rollovers/rollover3.wav");
	return true;
}

void PauseMenu::PlayClick(void)
{
	SCI->PlaySample("Assets/Sounds/Effects/GUI/Expands/map_submenu_popup.wav");
}