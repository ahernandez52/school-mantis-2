#include "HUD.h"
#include "PauseMenu.h"
#include "MantisGUI.h"
#include "../../FRAMEWORK/Managers/WorldManager.h"
#include "../../FRAMEWORK/Utility/Camera.h"
#include "../../GAMEPLAY/Object/Player.h"
#include "../../GAMEPLAY/Object/Ship.h"
#include "../../AI/AIObject/ShipAI.h"
#include "../../FRAMEWORK/Sound/Audio.h"

int HUD::m_InstanceNumber = 0;
int HUD::m_TargetableNumber = 0;

using namespace std;

inline D3DXVECTOR2 __GetPositionInScreenSpace(D3DXVECTOR3 position)
{
	D3DXVECTOR3 screenPos;

	D3DVIEWPORT9 viewport;
	gd3dDevice->GetViewport(&viewport);

	D3DXMATRIX world;
	D3DXMatrixIdentity(&world);

	D3DXVec3Project(&screenPos, &position, &viewport, &gCamera->proj(), &gCamera->view(), &world);

	return D3DXVECTOR2((float)((int)screenPos.x), (float)((int)screenPos.y));
}

inline D3DXVECTOR2 __GetClosestScreenPoint(D3DXVECTOR2 pos)
{
	POINT screenSize = gd3dApp->GetScreenSize();
	D3DXVECTOR2 closestPoint = pos;

	if(pos.x < 0)
		closestPoint.x = 32;
	if(pos.x > (float)screenSize.x)
		closestPoint.x = (float)screenSize.x - 32;

	if(pos.y < 0)
		closestPoint.y = 32;
	if(closestPoint.y > (float)screenSize.y)
		closestPoint.y = (float)screenSize.y - 32;

	return closestPoint;
}

inline D3DXVECTOR2 __GetClosestScreenPointSmall(D3DXVECTOR2 pos)
{
	POINT screenSize = gd3dApp->GetScreenSize();
	D3DXVECTOR2 closestPoint = pos;

	if(pos.x < 0)
		closestPoint.x = 32;
	if(pos.x > (float)screenSize.x)
		closestPoint.x = (float)screenSize.x;

	if(pos.y < 0)
		closestPoint.y = 32;
	if(closestPoint.y > (float)screenSize.y)
		closestPoint.y = (float)screenSize.y;

	return closestPoint;
}

inline std::string __ConvertUnitsToMeters(float num)
{
	std::string dist;

	float numMeters = num * 20.0f / 60.0f;

	if(numMeters >= 1000)
	{
		int km = (int)numMeters / 1000;
		int remainder = ((int)numMeters % 1000) / 100;

		dist = to_string(km) + "." + to_string(remainder) + "km";
	}
	else
	{
		dist = to_string((int)numMeters) + "m";
	}

	return dist;
}

HUD::HUD(MantisGUI* owner) : m_Owner(owner)
{
	m_HUDWindow = NULL;
	m_NamePrefix = "";
	m_Player = nullptr;
	m_CurrentTarget = nullptr;
	m_Display = false;
	m_PlayerReticle = false;
}

HUD::~HUD(void)
{
	ClearHUD();

	for(TargetableUnit* unit : m_TargetableUnits)
	{
		TargetableUnit* temp = unit;
		unit = nullptr;

		temp->actor = nullptr;
		m_HUDWindow->removeChildWindow(temp->window);
		CEGUI::WindowManager::getSingleton().destroyWindow(temp->window);

		delete temp;
	}

	CEGUI::System::getSingleton().getGUISheet()->removeChildWindow(m_HUDWindow);
	CEGUI::WindowManager::getSingleton().destroyWindow(m_HUDWindow);
}

//========================================
//  Public Functions
//========================================

void HUD::UpdateWaveMarker(float timeTillNextWave)
{
	m_HUDWindow->getChild("HUDRoot/WaveTimer")->setText("Next Wave In... " + to_string(timeTillNextWave));
}

void HUD::SetWaveNumber(int num)
{
	m_HUDWindow->getChild("HUDRoot/WaveTimer")->setText("Wave: " + to_string(num));
}

void HUD::UpdateScore(int score)
{
	m_HUDWindow->getChild("HUDRoot/PlayerScore")->setText("Score: " + to_string(score));
}

void HUD::Update(float dt)
{
	if(!isVisible())
	{
		return;
	}

	if(gRawInput->keyPressed(VK_SPACE))
	{
		if(m_Player->m_Steering)
		{
			SetReticleVisible(false);
		}
		else
		{
			SetReticleVisible(true);
		}
	}

	if(m_PlayerReticle)
	{
		//Get cursor position
		POINT p;
		GetCursorPos(&p);
		ScreenToClient(gd3dApp->getMainWnd(), &p);

		m_HUDWindow->getChild("HUDRoot/PlayerReticle")->setPosition(CEGUI::UVector2(CEGUI::UDim(0,(float)p.x - 24), CEGUI::UDim(0,(float)p.y - 24)));
	}

	if(m_Player)
	{
		UpdateScore(m_Player->m_Credits);

		static_cast<CEGUI::ProgressBar*>(m_HUDWindow->getChild("HUDRoot/HUDStatus")->getChild("HUDRoot/HUDStatus/ShieldBar"))->setProgress(m_Player->m_CurrentShield / m_Player->m_MaxShield);
		static_cast<CEGUI::ProgressBar*>(m_HUDWindow->getChild("HUDRoot/HUDStatus")->getChild("HUDRoot/HUDStatus/HullBar"))->setProgress(m_Player->m_CurrentHull / m_Player->m_MaxHull);
	}

	UpdateTargetReticle();

	m_HUDWindow->getChild("HUDRoot/HUDMenu")->getChild("HUDRoot/HUDMenu/MenuButton")->setTooltipText("FPS: " + to_string((int)m_Owner->GetGraphics()->GetFPS()));

	CreateTargetableUnits();

	UpdateTargetableUnits();

	RemoveTargetableUnits();
}

void HUD::Init(void)
{
	CreateCEGUIWindow();
	setVisible(true);
	m_HUDWindow->getChild("HUDRoot/HUDTarget")->hide();
	m_HUDWindow->getChild("HUDRoot/HUDLead")->hide();
	m_HUDWindow->getChild("HUDRoot/HUDTarget")->getChild("HUDRoot/HUDTarget/TargetArrow")->hide();
	m_HUDWindow->getChild("HUDRoot/PlayerReticle")->hide();
}

void HUD::setVisible(bool visible)
{
	m_Display = visible;

	if(m_Display)
	{
		m_HUDWindow->activate();
		m_HUDWindow->show();
		SetReticleVisible(false);
	}
	else
	{
		m_HUDWindow->deactivate();
		m_HUDWindow->hide();
		CEGUI::MouseCursor::getSingleton().show();
	}
}

bool HUD::isVisible(void)
{
	return m_Display;
}

void HUD::ToggleVisible()
{
	if(isVisible())
	{
		setVisible(false);
	}
	else
	{
		setVisible(true);
	}
}

void HUD::SetReticleVisible(bool visible)
{
	m_PlayerReticle = visible;

	if(m_PlayerReticle)
	{
		m_HUDWindow->getChild("HUDRoot/PlayerReticle")->show();
		CEGUI::MouseCursor::getSingleton().hide();
	}
	else
	{
		m_HUDWindow->getChild("HUDRoot/PlayerReticle")->hide();
		CEGUI::MouseCursor::getSingleton().show();
	}
}

void HUD::ToggleReticle()
{
	if(m_PlayerReticle)
	{
		SetReticleVisible(false);
	}
	else
	{
		SetReticleVisible(true);
	}
}

void HUD::ClearHUD()
{
	m_Player = nullptr;

	RemoveTarget();

	for(int x = m_TargetableUnits.size() - 1; x >= 0; --x)
	{
		m_HUDWindow->removeChildWindow(m_TargetableUnits[x]->window);
		CEGUI::WindowManager::getSingleton().destroyWindow(m_TargetableUnits[x]->window);

		TargetableUnit* temp = m_TargetableUnits[x];

		m_TargetableUnits.erase(m_TargetableUnits.begin() + x);

		delete temp;
	}

	CEGUI::MultiColumnList* mcl = static_cast<CEGUI::MultiColumnList*>(m_HUDWindow->getChild("HUDRoot/HUDTargetInfo")->getChild("HUDRoot/HUDTargetInfo/List"));

	mcl->resetList();
}

void HUD::SetPlayer(Player* player)
{
	m_Player = player;
}

void HUD::SelectTarget()
{
	std::vector<Actor*> actors = WMI->GetActors();
	std::vector<Actor*> ships;

	//get all the ships in the scene except the player and projectiles and no_type objects
	for (const auto actor : actors)
	{
		if(actor->Gameplay != nullptr)
		{
			if (actor != m_Player->GetActor() &&
				actor->Gameplay->GetGameplayType() != PROJECTILE &&
				actor->Gameplay->GetGameplayType() != NO_TYPE )
			{
				ships.push_back(actor);
			}
		}
	}

	if(ships.size() == 0)
	{
		return;
	}

	//Get cursor position
	POINT p;
	GetCursorPos(&p);
	ScreenToClient(gd3dApp->getMainWnd(), &p);
	
	D3DXVECTOR2 cursorPos;
	cursorPos.x = (float)p.x;
	cursorPos.y = (float)p.y;

	Actor* chosenActor = nullptr;
	D3DXVECTOR2 chosenActorScreenPos;

	for(const auto ship : ships)
	{
		if(!chosenActor)
		{
			chosenActor = ship;
			chosenActorScreenPos = __GetPositionInScreenSpace(chosenActor->m_Position);
		}
		else
		{
			D3DXVECTOR2 shipScreenPos = __GetPositionInScreenSpace(ship->m_Position);
			if(D3DXVec2LengthSq(&(shipScreenPos - cursorPos)) < D3DXVec2LengthSq(&(chosenActorScreenPos - cursorPos)))
			{
				chosenActor = ship;
				chosenActorScreenPos = shipScreenPos;
			}
		}
	}

	for(std::vector<TargetableUnit*>::iterator it = m_TargetableUnits.begin(); it != m_TargetableUnits.end(); ++it)
	{
		if((*it)->actor == m_CurrentTarget)
		{
			(*it)->window->show();
		}
	}

	SCI->PlaySample("Assets/Sounds/Effects/targeting.wav", 1.0f);

	m_CurrentTarget = chosenActor;
	m_Player->m_Target = chosenActor;

	for(std::vector<TargetableUnit*>::iterator it = m_TargetableUnits.begin(); it != m_TargetableUnits.end(); ++it)
	{
		if((*it)->actor == m_CurrentTarget)
		{
			(*it)->window->hide();

			CEGUI::MultiColumnList* mcl = static_cast<CEGUI::MultiColumnList*>(m_HUDWindow->getChild("HUDRoot/HUDTargetInfo")->getChild("HUDRoot/HUDTargetInfo/List"));
			mcl->clearAllSelections();
			mcl->setItemSelectState(CEGUI::MCLGridRef((*it)->listID, 1), true);
		}
	}

	if(m_CurrentTarget->Gameplay != nullptr)
	{
		switch(m_CurrentTarget->Gameplay->GetFaction())
		{
		case PHALANX:
			{
				CEGUI::Window* factionWindow = m_HUDWindow->getChild("HUDRoot/HUDTarget")->getChild("HUDRoot/HUDTarget/TargetFaction");
				factionWindow->setProperty("Image", "set:MantisLook image:PhalanxSymbol");
				break;
			}
		case ORDER:
			{
				CEGUI::Window* factionWindow = m_HUDWindow->getChild("HUDRoot/HUDTarget")->getChild("HUDRoot/HUDTarget/TargetFaction");
				factionWindow->setProperty("Image", "set:MantisLook image:OrderSymbol");
				break;
			}
		case TAAC:
			{
				CEGUI::Window* factionWindow = m_HUDWindow->getChild("HUDRoot/HUDTarget")->getChild("HUDRoot/HUDTarget/TargetFaction");
				factionWindow->setProperty("Image", "set:MantisLook image:TAACSymbol");
				break;
			}
		case PIRATE:
			{
				CEGUI::Window* factionWindow = m_HUDWindow->getChild("HUDRoot/HUDTarget")->getChild("HUDRoot/HUDTarget/TargetFaction");
				factionWindow->setProperty("Image", "set:MantisLook image:PirateSymbol");
				break;
			}
			break;
		}

		CEGUI::Window* levelWnd = m_HUDWindow->getChild("HUDRoot/HUDTarget")->getChild("HUDRoot/HUDTarget/TargetLevel");
		levelWnd->setText(to_string(1));
	}
	else
	{
		m_HUDWindow->getChild("HUDRoot/HUDTarget")->getChild("HUDRoot/HUDTarget/TargetFaction")->hide();
		m_HUDWindow->getChild("HUDRoot/HUDTarget")->getChild("HUDRoot/HUDTarget/TargetLevel")->hide();
		m_HUDWindow->getChild("HUDRoot/HUDTarget")->getChild("HUDRoot/HUDTarget/TargetShield")->hide();
		m_HUDWindow->getChild("HUDRoot/HUDTarget")->getChild("HUDRoot/HUDTarget/TargetHull")->hide();
	}

	

	m_HUDWindow->getChild("HUDRoot/HUDTarget")->show();
	m_HUDWindow->getChild("HUDRoot/HUDLead")->show();
}

void HUD::RemoveTarget()
{
	m_CurrentTarget = nullptr;
	m_HUDWindow->getChild("HUDRoot/HUDTarget")->hide();
	m_HUDWindow->getChild("HUDRoot/HUDLead")->hide();

	CEGUI::MultiColumnList* mcl = static_cast<CEGUI::MultiColumnList*>(m_HUDWindow->getChild("HUDRoot/HUDTargetInfo")->getChild("HUDRoot/HUDTargetInfo/List"));
	mcl->clearAllSelections();
	mcl->setSelectionMode(CEGUI::MultiColumnList::SelectionMode::RowSingle);
}

D3DXVECTOR2 HUD::ClampTargetReticle(D3DXVECTOR3 targetPosition)
{
	CEGUI::Window* targetWnd = m_HUDWindow->getChild("HUDRoot/HUDTarget");

	//get target in screen space
	D3DXVECTOR2 screenPos = __GetPositionInScreenSpace(targetPosition);
	D3DXVECTOR2 finalScreenCoords;

	POINT screenSize = gd3dApp->GetScreenSize();

	D3DXVECTOR2 screenMidPoint;
	screenMidPoint.x = (float)screenSize.x / 2;
	screenMidPoint.y = (float)screenSize.y / 2;

	Vector3 cameraLook, cameraUp, cameraRight, cameraPos;

	cameraLook = gCamera->look();
	cameraUp = gCamera->up();
	cameraRight = gCamera->right();
	cameraPos = gCamera->pos();

	Vector3 targetDir;
	targetDir = (targetPosition - cameraPos);
	targetDir.Normalize();
	
	float angle = acos( targetDir.Dot(cameraLook) );
	
	//check if the target is in screen space and in front of the camera
	if((0 <= screenPos.x && screenPos.x <= (float)screenSize.x) &&
		(0 <= screenPos.y && screenPos.y <= (float)screenSize.y) &&
		(angle < D3DX_PI / 2))
	{
		//if it is then just get the screen coords
		finalScreenCoords = screenPos;
	
		finalScreenCoords.x -= 64;
		finalScreenCoords.y -= 64;
	
		//show what we need to show
		targetWnd->getChild("HUDRoot/HUDTarget/TargetReticle")->show();
		targetWnd->getChild("HUDRoot/HUDTarget/TargetFaction")->show();
		targetWnd->getChild("HUDRoot/HUDTarget/TargetDistance")->show();
		targetWnd->getChild("HUDRoot/HUDTarget/TargetLevel")->show();
		targetWnd->getChild("HUDRoot/HUDTarget/TargetShield")->show();
		targetWnd->getChild("HUDRoot/HUDTarget/TargetHull")->show();
		m_HUDWindow->getChild("HUDRoot/HUDLead")->show();
		//hide what we need to hide
		targetWnd->getChild("HUDRoot/HUDTarget/TargetArrow")->hide();

		return finalScreenCoords;
	}
	else
	{
		Vector3 point;
	
		float d = cameraPos.Dot(cameraLook);

		float f = cameraLook.Dot(targetPosition) - d;

		point = targetPosition - f * cameraLook;

		point -= cameraPos;

		float x = point.Dot(cameraRight);
		float y = point.Dot(cameraUp);

		finalScreenCoords = __GetPositionInScreenSpace(point);

		if(finalScreenCoords.x < 0)
			finalScreenCoords.x = 32;
		if((float)screenSize.x < finalScreenCoords.x)
			finalScreenCoords.x = (float)screenSize.x - 32;
		if(finalScreenCoords.y < 0)
			finalScreenCoords.y = 32;
		if((float)screenSize.y < finalScreenCoords.y)
			finalScreenCoords.y = (float)screenSize.y - 32;

		if(abs(x) < abs(y))
		{
			//top
			if(y >= 0)
			{
				finalScreenCoords.y = 32;
				targetWnd->getChild("HUDRoot/HUDTarget/TargetArrow")->setProperty("Image", "set:MantisLook image:RedBigArrowUp");
			}
			//bottom
			else
			{
				finalScreenCoords.y = (float)screenSize.y - 32;
				targetWnd->getChild("HUDRoot/HUDTarget/TargetArrow")->setProperty("Image", "set:MantisLook image:RedBigArrowDown");
			}
		}
		else
		{
			//left
			if(x <= 0)
			{
				finalScreenCoords.x = 32;
				targetWnd->getChild("HUDRoot/HUDTarget/TargetArrow")->setProperty("Image", "set:MantisLook image:RedBigArrowLeft");
			}
			//right
			else
			{
				finalScreenCoords.x = (float)screenSize.x - 32;
				targetWnd->getChild("HUDRoot/HUDTarget/TargetArrow")->setProperty("Image", "set:MantisLook image:RedBigArrowRight");
			}
		}

		//hide things we dont need
		targetWnd->getChild("HUDRoot/HUDTarget/TargetReticle")->hide();
		targetWnd->getChild("HUDRoot/HUDTarget/TargetFaction")->hide();
		targetWnd->getChild("HUDRoot/HUDTarget/TargetDistance")->hide();
		targetWnd->getChild("HUDRoot/HUDTarget/TargetLevel")->hide();
		targetWnd->getChild("HUDRoot/HUDTarget/TargetShield")->hide();
		targetWnd->getChild("HUDRoot/HUDTarget/TargetHull")->hide();
		m_HUDWindow->getChild("HUDRoot/HUDLead")->hide();
		//show waht we need to show
		targetWnd->getChild("HUDRoot/HUDTarget/TargetArrow")->show();

		finalScreenCoords.x -= 64;
		finalScreenCoords.y -= 64;

		return finalScreenCoords;
	}
}

//========================================
//  Private Functions
//========================================

void HUD::CreateCEGUIWindow(void)
{
	//Get a local pointer to the window manager
	CEGUI::WindowManager* pWindowManager = CEGUI::WindowManager::getSingletonPtr();

	//Set and increase instance id
	m_NamePrefix = ++m_InstanceNumber + "_";

	//Create the HUD window and assign it to window manager
	m_HUDWindow = pWindowManager->loadWindowLayout("HUD.layout", m_NamePrefix);

	if (m_HUDWindow)
	{
		//If window succeeds attach it
		CEGUI::System::getSingleton().getGUISheet()->addChildWindow(m_HUDWindow);

		//Register handlers for events
		(this)->RegisterHandlers();
	}

	else
	{
		//Log a failed event
		CEGUI::Logger::getSingleton().logEvent("Error: Unable to load the HUD window from .layout");
	}
}

void HUD::RegisterHandlers(void)
{
	m_HUDWindow->getChild(m_NamePrefix + "HUDRoot/HUDMenu")->getChild("HUDRoot/HUDMenu/MenuButton")->subscribeEvent(
		CEGUI::PushButton::EventClicked,
		CEGUI::Event::Subscriber(
		&HUD::Handle_MenuButtonPressed, this));

	m_HUDWindow->getChild(m_NamePrefix + "HUDRoot/HUDMenu")->subscribeEvent(
		CEGUI::Window::EventMouseEntersArea,
		CEGUI::Event::Subscriber(
		&HUD::Handle_MenuHover, this));

	m_HUDWindow->getChild(m_NamePrefix + "HUDRoot/HUDMenu")->subscribeEvent(
		CEGUI::Window::EventMouseLeavesArea,
		CEGUI::Event::Subscriber(
		&HUD::Handle_MenuUnHover, this));
}

bool HUD::Handle_MenuButtonPressed(const CEGUI::EventArgs& e)
{
	ToggleVisible();
	m_Owner->GetFramework()->TogglePause();
	m_Owner->m_PauseMenu->ToggleVisible();
	
	return true;
}

bool HUD::Handle_MenuHover(const CEGUI::EventArgs& e)
{
	m_HUDWindow->getChild("HUDRoot/HUDMenu")->setProperty("Alpha", "1");

	return true;
}

bool HUD::Handle_MenuUnHover(const CEGUI::EventArgs& e)
{
	m_HUDWindow->getChild("HUDRoot/HUDMenu")->setProperty("Alpha", "0.5");

	return true;
}

void HUD::UpdateTargetReticle()
{
	if(!m_Player->m_Target)
	{
		RemoveTarget();
	}

	if(m_CurrentTarget)
	{
		if(D3DXVec3LengthSq(&(m_CurrentTarget->m_Position - m_Player->GetActor()->m_Position)) >= 8000 * 8000)
		{
			RemoveTarget();
		}
	}

	if(m_CurrentTarget)
	{
		D3DXVECTOR2 pos = ClampTargetReticle(m_CurrentTarget->m_Position);

		m_HUDWindow->getChild("HUDRoot/HUDTarget")->setPosition(CEGUI::UVector2(CEGUI::UDim(0,pos.x), CEGUI::UDim(0,pos.y)));

		float dist = D3DXVec3Length(&(m_CurrentTarget->m_Position - m_Player->GetActor()->m_Position));

		m_HUDWindow->getChild("HUDRoot/HUDTarget")->getChild("HUDRoot/HUDTarget/TargetDistance")->setText(__ConvertUnitsToMeters(dist));
		
		ShipAI* targetShipAI = static_cast<ShipAI*>(m_CurrentTarget->AI);

		if(targetShipAI)
		{
			Vector3 playerToTarget = m_CurrentTarget->m_Position - m_Player->GetActor()->m_Position;

			float projectileSpeed = 2000;
			float leadDist = playerToTarget.Length() / ( projectileSpeed + m_CurrentTarget->AI->GetCurrentVelocity().Length());

			Vector3 targetPos = m_CurrentTarget->m_Position + targetShipAI->GetCurrentVelocity() * leadDist;

			D3DXVECTOR2 leadPos = __GetPositionInScreenSpace(targetPos);

			leadPos.x -= 24;
			leadPos.y -= 24;

			m_HUDWindow->getChild("HUDRoot/HUDLead")->setPosition(CEGUI::UVector2(CEGUI::UDim(0,leadPos.x), CEGUI::UDim(0,leadPos.y)));
		}
		else
		{
			D3DXVECTOR2 leadPos = pos;
			leadPos.x -= 24;
			leadPos.y -= 24;

			m_HUDWindow->getChild("HUDRoot/HUDLead")->setPosition(CEGUI::UVector2(CEGUI::UDim(0,leadPos.x), CEGUI::UDim(0,leadPos.y)));
		}

		Ship* targetShip = static_cast<Ship*>(m_CurrentTarget->Gameplay);
		
		if(targetShip)
		{
			static_cast<CEGUI::ProgressBar*>(m_HUDWindow->getChild("HUDRoot/HUDTarget")->getChild("HUDRoot/HUDTarget/TargetShield"))->setProgress(targetShip->m_CurrentShield / targetShip->m_MaxShield);
			static_cast<CEGUI::ProgressBar*>(m_HUDWindow->getChild("HUDRoot/HUDTarget")->getChild("HUDRoot/HUDTarget/TargetHull"))->setProgress(targetShip->m_CurrentHull / targetShip->m_MaxHull);
		}
		else
		{
			m_HUDWindow->getChild("HUDRoot/HUDTarget")->getChild("HUDRoot/HUDTarget/TargetFaction")->hide();
			m_HUDWindow->getChild("HUDRoot/HUDTarget")->getChild("HUDRoot/HUDTarget/TargetLevel")->hide();
			m_HUDWindow->getChild("HUDRoot/HUDTarget")->getChild("HUDRoot/HUDTarget/TargetShield")->hide();
			m_HUDWindow->getChild("HUDRoot/HUDTarget")->getChild("HUDRoot/HUDTarget/TargetHull")->hide();
		}
	}
}

void HUD::CreateTargetableUnits()
{
	vector<Ship*> ships = m_Owner->GetFramework()->GetGameplay()->GetNPCs();

	for(Ship* ship : ships)
	{
		if(ship->GetActor() == m_CurrentTarget || ship->GetActor() == m_Player->GetActor())
		{
			continue;
		}

		bool doContinue = false;

		for(TargetableUnit* unit : m_TargetableUnits)
		{
			if(unit->actor == ship->GetActor())
			{
				doContinue = true;
				continue;
			}
		}

		if(doContinue)
		{
			continue;
		}

		if(D3DXVec3LengthSq(&(ship->GetActor()->m_Position - m_Player->GetActor()->m_Position)) <= 8000 * 8000)
		{
			TargetableUnit* temp = new TargetableUnit();

			//Get a local pointer to the window manager
			CEGUI::WindowManager* pWindowManager = CEGUI::WindowManager::getSingletonPtr();
			//create name for window
			CEGUI::String targetableName = "HUDRoot/TargetableReticule" + to_string(++m_TargetableNumber) + "/";
			//Create the HUD window and assign it to window manager
			CEGUI::Window* targetableUnitWindow = pWindowManager->loadWindowLayout("HUDTargetable.layout", targetableName);

			if (targetableUnitWindow)
			{
				//If window succeeds attach it
				m_HUDWindow->addChildWindow(targetableUnitWindow);
			}

			else
			{
				//Log a failed event
				CEGUI::Logger::getSingleton().logEvent("Error: Unable to load the Targetable window " + targetableName + " from .layout");
			}

			D3DXVECTOR2 windowPosition = __GetPositionInScreenSpace(ship->GetActor()->m_Position);

			targetableUnitWindow->setPosition(CEGUI::UVector2(CEGUI::UDim(0, windowPosition.x - 32), CEGUI::UDim(0, windowPosition.y - 32)));

			targetableUnitWindow->show();

			temp->window = targetableUnitWindow;
			temp->actor = ship->GetActor();

			m_TargetableUnits.push_back(temp);

			//add it to the mclbox
			CEGUI::MultiColumnList* mcl = static_cast<CEGUI::MultiColumnList*>(m_HUDWindow->getChild("HUDRoot/HUDTargetInfo")->getChild("HUDRoot/HUDTargetInfo/List"));

			temp->listID = mcl->addRow();

			temp->targetInfoName = new CEGUI::ListboxTextItem("Pirate");
			temp->targetInfoName->setSelectionBrushImage("TaharezLook", "MultiListSelectionBrush");

			mcl->setItem(temp->targetInfoName, 0, temp->listID);

			float dist = D3DXVec3Length(&(temp->actor->m_Position - m_Player->GetActor()->m_Position));

			temp->targetInfoDistance = new CEGUI::ListboxTextItem(__ConvertUnitsToMeters(dist));
			temp->targetInfoDistance->setSelectionBrushImage("TaharezLook", "MultiListSelectionBrush");

			mcl->setItem(temp->targetInfoDistance, 1, temp->listID);
		}
	}
}

void HUD::UpdateTargetableUnits()
{
	CEGUI::MultiColumnList* mcl = static_cast<CEGUI::MultiColumnList*>(m_HUDWindow->getChild("HUDRoot/HUDTargetInfo")->getChild("HUDRoot/HUDTargetInfo/List"));

	for(TargetableUnit* unit : m_TargetableUnits)
	{
		D3DXVECTOR2 windowPosition = ClampSecondaryReticle(unit);

		unit->window->setPosition(CEGUI::UVector2(CEGUI::UDim(0, windowPosition.x), CEGUI::UDim(0, windowPosition.y)));

		float dist = D3DXVec3Length(&(unit->actor->m_Position - m_Player->GetActor()->m_Position));
		
		unit->targetInfoDistance->setText(__ConvertUnitsToMeters(dist));
	}

	mcl->handleUpdatedItemData();
}

void HUD::RemoveTargetableUnits()
{
	for(int x = m_TargetableUnits.size() - 1; x >= 0; --x)
	{
		if(D3DXVec3LengthSq(&(m_TargetableUnits[x]->actor->m_Position - m_Player->GetActor()->m_Position)) > 8000 * 8000 || !m_TargetableUnits[x]->actor)
		{
			m_HUDWindow->removeChildWindow(m_TargetableUnits[x]->window);
			CEGUI::WindowManager::getSingleton().destroyWindow(m_TargetableUnits[x]->window);

			TargetableUnit* temp = m_TargetableUnits[x];

			m_TargetableUnits.erase(m_TargetableUnits.begin() + x);

			delete temp;

			RePopulateTargetLog();
		}
	}
}

void HUD::RePopulateTargetLog()
{
	CEGUI::MultiColumnList* mcl = static_cast<CEGUI::MultiColumnList*>(m_HUDWindow->getChild("HUDRoot/HUDTargetInfo")->getChild("HUDRoot/HUDTargetInfo/List"));

	mcl->resetList();

	for(TargetableUnit* unit : m_TargetableUnits)
	{
		unit->listID = mcl->addRow();

		mcl->setItem(new CEGUI::ListboxTextItem("Pirate"), 0, unit->listID);

		float dist = D3DXVec3Length(&(unit->actor->m_Position - m_Player->GetActor()->m_Position));
		unit->targetInfoDistance = new CEGUI::ListboxTextItem(__ConvertUnitsToMeters(dist));

		mcl->setItem(unit->targetInfoDistance, 1, unit->listID);
	}
}

D3DXVECTOR2 HUD::ClampSecondaryReticle(TargetableUnit* unit)
{
	CEGUI::Window* targetWnd = unit->window;
	CEGUI::String windowName = targetWnd->getName();

	//get target in screen space
	D3DXVECTOR2 screenPos = __GetPositionInScreenSpace(unit->actor->m_Position);
	D3DXVECTOR2 finalScreenCoords;

	POINT screenSize = gd3dApp->GetScreenSize();

	D3DXVECTOR2 screenMidPoint;
	screenMidPoint.x = (float)screenSize.x / 2;
	screenMidPoint.y = (float)screenSize.y / 2;

	Vector3 cameraLook, cameraUp, cameraRight, cameraPos;

	cameraLook = gCamera->look();
	cameraUp = gCamera->up();
	cameraRight = gCamera->right();
	cameraPos = gCamera->pos();

	Vector3 targetDir;
	targetDir = (unit->actor->m_Position - cameraPos);
	targetDir.Normalize();
	
	float angle = acos( targetDir.Dot(cameraLook) );
	
	//check if the target is in screen space and in front of the camera
	if((0 <= screenPos.x && screenPos.x <= (float)screenSize.x) &&
		(0 <= screenPos.y && screenPos.y <= (float)screenSize.y) &&
		(angle < D3DX_PI / 2))
	{
		//if it is then just get the screen coords
		finalScreenCoords = screenPos;
	
		finalScreenCoords.x -= 32;
		finalScreenCoords.y -= 32;
	
		targetWnd->getChild(windowName + "Reticle")->setProperty("Image", "set:MantisLook image:EnemySelectableTarget");
		targetWnd->setProperty("UnifiedSize", "{{0,64},{0,64}}");

		return finalScreenCoords;
	}
	else
	{
		Vector3 point;
	
		float d = cameraPos.Dot(cameraLook);

		float f = cameraLook.Dot(unit->actor->m_Position) - d;

		point = unit->actor->m_Position - f * cameraLook;

		point -= cameraPos;

		float x = point.Dot(cameraRight);
		float y = point.Dot(cameraUp);

		finalScreenCoords = __GetPositionInScreenSpace(point);

		if(finalScreenCoords.x < 0)
			finalScreenCoords.x = 32;
		if((float)screenSize.x < finalScreenCoords.x)
			finalScreenCoords.x = (float)screenSize.x - 32;
		if(finalScreenCoords.y < 0)
			finalScreenCoords.y = 32;
		if((float)screenSize.y < finalScreenCoords.y)
			finalScreenCoords.y = (float)screenSize.y - 32;

		if(abs(x) < abs(y))
		{
			//top
			if(y >= 0)
			{
				finalScreenCoords.y = 32;
				targetWnd->getChild(windowName + "Reticle")->setProperty("Image", "set:MantisLook image:RedSmallArrowUp");
			}
			//bottom
			else
			{
				finalScreenCoords.y = (float)screenSize.y;
				targetWnd->getChild(windowName + "Reticle")->setProperty("Image", "set:MantisLook image:RedSmallArrowDown");
			}
		}
		else
		{
			//left
			if(x <= 0)
			{
				finalScreenCoords.x = 32;
				targetWnd->getChild(windowName + "Reticle")->setProperty("Image", "set:MantisLook image:RedSmallArrowLeft");
			}
			//right
			else
			{
				finalScreenCoords.x = (float)screenSize.x;
				targetWnd->getChild(windowName + "Reticle")->setProperty("Image", "set:MantisLook image:RedSmallArrowRight");
			}
		}

		targetWnd->setProperty("UnifiedSize", "{{0,32},{0,32}}");

		finalScreenCoords.x -= 32;
		finalScreenCoords.y -= 32;

		return finalScreenCoords;
	}
}