#pragma once
#include "d3dApp.h"
#include <tchar.h>
#include <crtdbg.h>
#include "GameStateMachine.h"
#include "RawInput.h"
#include "XBOXController.h"
//#include "Utility\Camera.h"

//cores
//#include "../GRAPHICS/D3D9GraphicsCore.h"
//#include "../AI/AICore.h"
//#include "../PHYSICS/DragonPhysicsEngine.h"
//#include "../GAMEPLAY/GameplayCore.h"
#include "GraphicsCore.h"

using namespace DX9GraphicsCore;

class GameFramework : public D3DApp
{
public:
	GameFramework(HINSTANCE hInstance, std::string winCaption, D3DDEVTYPE devType, DWORD requestedVP);
	~GameFramework();

	bool checkDeviceCaps();

	bool isPaused();
	void SetPause(bool b);
	void TogglePause();
	
	void onLostDevice();
	void onResetDevice();
	void updateScene(float dt);
	void drawScene();

	void startGSM();
	void startHUD();

	POINT GetScreenSize();

	GameStateMachine *getGSM();

	//MantisGUI* GetGui(void) override final;
	//D3D9GraphicsCore* GetGraphics(void){return m_GraphicsCore;}  //TEMPORARY FOR GUI
	//AICore*	GetAI(void){return m_AICore;}
	//GameplayCore* GetGameplay(void){return m_GameplayCore;}

private:
	GameStateMachine	*GSM;

	MantisGUI			*m_Gui;

	//Camera				*m_Camera;

	bool				 m_paused;

	GraphicsCore *m_GraphicsCore;

	//Cores
	//D3D9GraphicsCore	*m_GraphicsCore;
	//AICore				*m_AICore;
	//DragonPhysicsEngine	*m_PhysicsCore;
	//GameplayCore		*m_GameplayCore;

	void EventXBOXController();
};