#pragma once
//#include "Geometry.h"
#include "MathLibrary.h"
#include <string>

using namespace DXMathLibrary;

//Object Schematics
//===============================================================
struct AISchematic
{
	std::string			Identifier;
	std::string			Faction;
	std::string			AIType;
};

struct GraphicSchematic
{
	std::string			name;
	std::string			fileName;
	std::string			Shader;
	ID3DXMesh			*mesh;
	LPDIRECT3DTEXTURE9	maps[5];		//about to deprecate
	std::string			mapNames[5];
	DWORD				detailFlags;
};

struct PhysicsSchematic
{
	std::string			Identifier;
	int					Type;
	bool				Moveable;
	float				Mass;
	float				Radius;
	float				DistanceFromOrigin;
	Vector3				HalfExtents;
	Vector3				PlaneNormalDirection;

	std::string			MeshName;
	ID3DXMesh			*Mesh;
};
//***************************************************************