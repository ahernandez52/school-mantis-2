#pragma once
#include "GameState.h"
#include <thread>

class GameStateMachine{
private:

	GameState	*m_CurrentState;
	GameState	*m_LoadingState;
	GameState	*m_NextState;

	bool		 m_SwitchingStates;
	bool		 m_DoneLoadingNextState;

private:

	GameFramework			*m_GameFramework;
	D3D9GraphicsCore		*m_GraphicsCore;
	DragonPhysicsEngine		*m_PhysicsCore;
	AICore					*m_AICore;
	GameplayCore			*m_GameplayCore;
 
public:
	GameStateMachine()
	{
		m_CurrentState		= nullptr;
		m_LoadingState		= nullptr;
		m_NextState			= nullptr;

		m_SwitchingStates	= false;
		m_DoneLoadingNextState = false;
	}

	virtual ~GameStateMachine()
	{
		//Clean up state allocation
		if(m_CurrentState)
		{
			//call leave state
			m_CurrentState->LeaveState();
			//delete the state
			delete m_CurrentState;
			//set to nullptr
			m_CurrentState = nullptr;
		}

		if(m_LoadingState)
		{
			m_LoadingState->LeaveState();
			delete m_LoadingState;
			m_LoadingState = nullptr;
		}

		if(m_NextState)
		{
			m_NextState->LeaveState();
			delete m_NextState;
			m_NextState = nullptr;
		}
	}

	/**
		Sets the Current state the GSM will
		load into
	*/
	void SetCurrentState(GameState* gs)	
	{ 
		m_CurrentState = gs;

		m_CurrentState->Framework	= m_GameFramework;
		m_CurrentState->Graphics	= m_GraphicsCore;
		m_CurrentState->Physics		= m_PhysicsCore;
		m_CurrentState->AI			= m_AICore;
		m_CurrentState->Gameplay	= m_GameplayCore;
	}

	/**
		Sets the LoadingState that the GSM
		Will use when loading to new state
	*/
	void SetLodingState(GameState* gs)	
	{ 
		m_LoadingState = gs; 

		m_LoadingState->Framework	= m_GameFramework;
		m_LoadingState->Graphics	= m_GraphicsCore;
		m_LoadingState->Physics		= m_PhysicsCore;
		m_LoadingState->AI			= m_AICore;
		m_LoadingState->Gameplay	= m_GameplayCore;
	}

	/**
		Sets the next state the GSM
		Will load into
	*/
	void SetNextState(GameState* gs)	
	{ 
		m_NextState =gs; 

		m_NextState->Framework	= m_GameFramework;
		m_NextState->Graphics	= m_GraphicsCore;
		m_NextState->Physics	= m_PhysicsCore;
		m_NextState->AI			= m_AICore;
		m_NextState->Gameplay	= m_GameplayCore;
	}

	/**
		Initiates the loading of the next state
	*/
	void StartLoadingNextState()		{ m_NextState->InitializeState(); }
	void StartUnloadingCurrentState()	{ }

	//assign cores
	/**
		(REQUIRED) This function assigns the
		framework's active cores to the GSM
	*/
	void StartupGSM(GameFramework* framework, 
				    D3D9GraphicsCore *graphicsCore, 
					DragonPhysicsEngine *PhysicsCore, 
					AICore *aiCore,
					GameplayCore *gameplayCore)
	{
		m_GameFramework	= framework;
		m_GraphicsCore	= graphicsCore;
		m_PhysicsCore	= PhysicsCore;
		m_AICore		= aiCore;
		m_GameplayCore	= gameplayCore;
	}

	void LoadNextState()
	{
		m_SwitchingStates = true;

		GameState *StateHolder = m_CurrentState;

		m_CurrentState = m_LoadingState;
		m_LoadingState = StateHolder;
	}

	/**
		Called right after displaying
		a loading screen
	*/
	void LoadingNextState()
	{
		//Deallocate Previous State
		if(!(m_LoadingState == nullptr))
		{
			m_LoadingState->LeaveState();
			delete m_LoadingState;
		}

		//Start Loading NextState
		m_NextState->InitializeState();

		//Swap Loading state back intio m_LoadingState
		m_LoadingState = m_CurrentState;

		//Set Current State to next State
		m_CurrentState = m_NextState;

		//set nextState to nullptr
		m_NextState = nullptr;

		m_SwitchingStates = false;
	}

	/**
		Initates a state swap
	*/
	void SwapNextStateIn()
	{
		m_LoadingState = m_CurrentState;
		m_CurrentState = m_NextState;
		m_DoneLoadingNextState = false;
	}

	//Only works in threaded engine
	void StartLoadingNextState(GameState* gs)
	{
		//m_NextState = gs;
		//m_NextState->InitializeState();
	}

	/**
		makes the loading state visible
	*/
	void DisplayLoadingState()
	{
		m_CurrentState = m_LoadingState;
		m_CurrentState->EnterState();
	}

	/**
		Called to update the Current State
	*/
	void Update(float dt)
	{
		if(m_SwitchingStates)
			LoadingNextState();

		m_CurrentState->Update(dt);
	}

	GameState* GetCurrentState()
	{
		return m_CurrentState;
	}
};