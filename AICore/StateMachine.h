/**
 * State Machine - Templated state machine used for player states
 *
 * Author - Jesse Dillon
 */

#pragma once 

#include "BaseState.h"

namespace AI
{

	template <class owner_type>
	class StateMachine
	{
	public:

		owner_type* m_Owner;

		BaseState<owner_type>*	m_Current;
		BaseState<owner_type>*  m_Previous;


	};


}