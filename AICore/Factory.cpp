#include "Factory.h"

namespace AI
{


	Factory::Factory(void)
	{
	}

	Factory::~Factory(void)
	{
	}

	AIObject* Factory::CreateObject(Actor* owner)
	{
		return new AIObject(owner);
	}

	AIObject* Factory::CreateObject(Actor* owner, AI_TYPE type)
	{
		return new AIObject(owner);
	}

}

