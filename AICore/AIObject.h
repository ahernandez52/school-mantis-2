/**
 * AIObject - Base AI Object 
 *
 * Author - Jesse Dillon
 */

#pragma once

#include "../MathLibrary/MathLibrary.h"

class Actor;


using namespace DXMathLibrary;

//  <ALERT> Temporary Actor class for DLL building
class Actor
{
public:

	Vector3 m_Position;
	Vector3 m_Velocity;

	Quaternion m_Orientation;

private:

	Actor(void){}

	~Actor(void){}
};

namespace AI
{
	//Forward declaration
	struct Telegram;

	//Type specifier
	enum AI_TYPE
	{
		AI_UNDECLARED = 0,
		AI_SHIP		  = 1,
		AI_TURRET     = 2,
		AI_MISSILE    = 3, 
		AI_OBSTACLE   = 4,
		AI_PLAYER	  = 9
	};

	/**
	 * AIObject
	 */
	class __declspec(dllexport) AIObject
	{
	protected:

		//Owning actor
		Actor*		m_Actor;

		//Identifier
		int			m_Id;

		//AI type
		AI_TYPE     m_Type;

		//Object activity
		bool		m_Active;


	protected:

		AIObject(void);

	public:

		AIObject(Actor* owner);
		
		virtual ~AIObject(void);

		/**
		 * Update
		 */

		virtual void Update(float dt);

		/**
		 * Messaging
		 */

		virtual bool HandleMessage(Telegram m);

		/**
		 * Getters
		 */

		Actor*  GetSelf(void);

		int	    GetId(void);

		AI_TYPE GetType(void);

		bool	IsActive(void);

		//Physical properties
		Vector3 GetPosition(void) const;
		Vector3 GetVelocity(void) const;

		Quaternion GetOrientation(void) const;

		//Virtual size for non-moving AI objects
		virtual float GetSize(void){return 0.0f;}

		/**
		 * Setters
		 */

		void SetType(AI_TYPE type);

		void SetActive(bool active);

		//Physical properties
		void SetPosition(const Vector3 pos);
		void SetVelocity(const Vector3 vel);

		void SetOrientation(const Quaternion q);
	};


}