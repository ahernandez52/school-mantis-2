/**
 * MessageHandler - Processes messages sent between AI agents
 *
 * Author - Jesse Dillon
 */

#pragma once

#include "Telegram.h"
#include <set>

namespace AI
{

#define Messager MessageHandler::Instance()

	class AIObject;
	class AICore;

	class MessageHandler
	{
	private:

		//Pointer to the AI core
		AICore*			   m_Core;

		//Containter for delayed messages
		std::set<Telegram> m_Queue;

	public:

		static MessageHandler* Instance();

		~MessageHandler(void);

		void Initialize(AICore* core);

		void Shutdown(void);

		//Send message
		void Message(float delay, int sender, int receiver, MESSAGE msg, void* info);

		//Update message system
		void Update(float dt);

	private:

		//Call agent handler
		void Discharge(AIObject* agent, const Telegram& msg);

		void ClearMessages(void);

		MessageHandler(void);
	};


}