/**
 * BaseState - Base state used with the state machine
 *
 * Author - Jesse Dillon
 */

#pragma once

namespace AI
{

	template <class owner_type>
	class BaseState
	{
	public:
		BaseState(void);

		virtual ~BaseState(void);

		virtual void Initialize(owner_type*)       = 0;
		virtual void Update(owner_type*, float dt) = 0;
		virtual void LeaveState(owner_type*)       = 0;

		virtual void HandleMessage(owner_type*, Telegram* m) = 0;
		
	};
}