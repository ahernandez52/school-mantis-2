/**
 * Factory - Creates AIObjects and returns them
 *
 * Author - Jesse Dillon
 */

#pragma once

#include "AIObject.h"

class Actor;

namespace AI
{

	class Factory
	{
	private:

	public:

		Factory(void);
		~Factory(void);

		AIObject* CreateObject(Actor* owner);

		AIObject* CreateObject(Actor* owner, AI_TYPE type);
	};

}