/**
 * Message - Declares the message class which hold message information
 *
 * Author - Jesse Dillon
 */

#pragma once

#include <math.h>

namespace AI
{


	enum MESSAGE
	{
		NO_MESSAGE = -1,
		IDLE = 0,
		ATTACK = 1,
		EVADE = 2
	};

	struct Telegram
	{
		//Sender Id
		int		m_Sender;

		//Receiver Id
		int		m_Receiver;

		//Message
		MESSAGE m_Message;

		//Delivery Time
		float   m_Delivery;

		//Delay
		float   m_Delay;

		//Extra information
		void*   m_ExtraInfo;

		Telegram(float delay, int sender, int receiver, MESSAGE msg, void* info)
			: m_Delay(delay), m_Sender(sender), m_Receiver(receiver), 
			  m_Message(msg), m_ExtraInfo(info)
		{
		}

		~Telegram(void){}
	};


	//Overload "==" and "<" operators for set
	const float SmallestDelay = 0.25;

	inline bool operator == (const Telegram& t1, const Telegram& t2)
	{
		return (fabs(t1.m_Delivery - t2.m_Delivery) < SmallestDelay) &&
			(t1.m_Delivery == t2.m_Delivery) &&
			(t1.m_Receiver == t2.m_Receiver) &&
			(t1.m_Message  == t2.m_Message);
	}

	inline bool operator < (const Telegram& t1, const Telegram& t2)
	{
		if (t1 == t2)
			return false;
		else return (t1.m_Delivery < t2.m_Delivery);
	}
}
