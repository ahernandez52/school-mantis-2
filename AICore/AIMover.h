/**
 * AIMover - Declares an AIObject which contains movement capability
 *
 * Author - Jesse Dillon
 */

#pragma once

#include "AIObject.h"
#include "StateMachine.h"
#include "SteeringBehavior.h"



namespace AI
{


	struct Telegram;

	class AIMover : public AIObject
	{
	private:

		//State machine
		StateMachine<AIMover>* m_FSM;

	private:

		float m_Size;					//Ship radius
		float m_MaxRotation;			//Maximum turning radius
		float m_MaxAngularAcceleration; //Maximum angular acceleration 
		float m_MaxSpeed;				//Maximum speed

		Vector3 m_MaxForce;				//Maximum acceleration

		SteeringBehavior* m_Steering;	//Steering

	public:

		AIMover(void);
		AIMover(Actor* owner);
		
		virtual ~AIMover(void);

		/** 
		 * Update
		 */

		virtual void Update(float dt) override;
		virtual bool HandleMessage(Telegram m) override;
		
		/**
		 * Setters
		 */

		//Set state
		void SetState(BaseState<AIMover>* newState);

		//Set movement capabilities
		void SetMaxRotation(float rot);
		void SetMaxAngularAcceleration(float accel);
		
		void SetMaxSpeed(float speed);
		void SetMaxForce(Vector3 force);

		//Set size
		void SetSize(float radius);

		/**
		 * Getters
		 */

		float GetMaxRotation(void);
		float GetMaxAngularAcceleration(void);

		float GetMaxSpeed(void);
		Vector3 GetMaxForce(void);

		float GetSize(void);

		SteeringBehavior* GetSteering(void);

		

	};


}