/**
 * AgentManager - Singleton maintains map and vector of objects
 *
 * Author - Jesse Dillon
 */

#pragma once 

#include "AIObject.h"

#include <vector>
#include <map>

namespace AI
{

#define AMI AgentManager::Instance()

	class AIMover;

	class AgentManager
	{
	private:

		//Map to AI Objects
		std::map<int, AIObject*>	m_AgentMap;

		//Vector to AI Objects
		std::vector<AIObject*>		m_AgentVector;

	public:

		static AgentManager* Instance(void);

		~AgentManager(void);

		/**
		 * Accessors
		 */

		std::vector<AIObject*>&	GetAgents(void);

		std::vector<AIMover*> GetMovers(void);

		AIObject* GetAgentById(int id);


		/**
		 * Utilities
		 */

		void RegisterAgent(Actor* owner, AI_TYPE type);

		void ClearAgents(void);

	private:

		AgentManager(void);
	};


}


