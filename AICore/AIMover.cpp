#include "AIMover.h"
#include "MessageHandler.h"
#include "SteeringBehavior.h"



namespace AI
{


	AIMover::AIMover(void) : AIObject()
	{
		m_Size = 0.0f;
		m_MaxRotation = 0.0f;
		m_MaxAngularAcceleration = 0.0f;
		m_MaxSpeed = 0.0f;

		m_MaxForce = Vector3(0.0f, 0.0f, 0.0f);

		m_Steering = nullptr;

		m_FSM = nullptr;
	}

	AIMover::AIMover(Actor* owner) : AIObject(owner)
	{
		m_Size = 0.0f;
		m_MaxRotation = 0.0f;
		m_MaxAngularAcceleration = 0.0f;
		m_MaxSpeed = 0.0f;

		m_MaxForce = Vector3(0.0f, 0.0f, 0.0f);

		m_Steering = new SteeringBehavior(this);

		m_FSM = new StateMachine<AIMover>();
	}

	AIMover::~AIMover(void)
	{
		delete m_Steering;
		delete m_FSM;
	}

	/**
	 * Update
	 */

	void AIMover::Update(float dt)
	{

	}

	bool AIMover::HandleMessage(Telegram m)
	{
		return true;
	}

	/**
	 * Setters
	 */

	void AIMover::SetState(BaseState<AIMover>* newState)
	{
	}

	void AIMover::SetMaxRotation(float rot)
	{
	}

	void AIMover::SetMaxAngularAcceleration(float accel)
	{
	}

	void AIMover::SetMaxSpeed(float speed)
	{
	}

	void AIMover::SetMaxForce(Vector3 force)
	{
	}

	void AIMover::SetSize(float radius)
	{
	}

	/**
	 * Getters
	 */

	float AIMover::GetMaxRotation(void)
	{
		return m_MaxRotation;
	}

	float AIMover::GetMaxAngularAcceleration(void)
	{
		return m_MaxAngularAcceleration;
	}

	float AIMover::GetMaxSpeed(void)
	{
		return m_MaxSpeed;
	}

	Vector3 AIMover::GetMaxForce(void)
	{
		return m_MaxForce;
	}

	float AIMover::GetSize(void)
	{
		return m_Size;
	}

	SteeringBehavior* AIMover::GetSteering(void)
	{
		return m_Steering;
	}

}