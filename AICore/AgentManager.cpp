#include "AgentManager.h"
#include "Factory.h"

namespace AI
{


	AgentManager::AgentManager(void)
	{
	}

	AgentManager* AgentManager::Instance(void)
	{
		static AgentManager temp;
		return &temp;
	}

	AgentManager::~AgentManager(void)
	{
		ClearAgents();
	}

	void AgentManager::RegisterAgent(Actor* owner, AI_TYPE type)
	{
		Factory builder;

		AIObject* temp = builder.CreateObject(owner, type);

		m_AgentVector.push_back(temp);
		m_AgentMap.insert(std::make_pair(temp->GetId(), temp));
	}

	void AgentManager::ClearAgents(void)
	{
		for (auto& agent : m_AgentVector)
		{
			if (agent)
			{
				AIObject* temp = agent;
				agent = nullptr;
				delete temp;
			}
		}

		m_AgentVector.clear();
		m_AgentMap.clear();
	}

	std::vector<AIObject*>& AgentManager::GetAgents(void)
	{
		return m_AgentVector;
	}

	std::vector<AIMover*> AgentManager::GetMovers(void)
	{
		std::vector<AIMover*> movers;

		for (auto& agent : m_AgentVector)
		{
			AI_TYPE type = agent->GetType();

			if ((type == AI_OBSTACLE) ||
				(type == AI_UNDECLARED) ||
				(type == AI_TURRET))
			{
				continue;
			}

			movers.push_back((AIMover*)agent);
		}

		return movers;
	}

	AIObject* AgentManager::GetAgentById(int id)
	{
		return m_AgentMap.find(id)->second;
	}


}