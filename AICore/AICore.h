/**
 * AICore - The core manages AI entities and serves as the main access point
 * for the remainder of the application
 * 
 * Author - Jesse Dillon
 */

#pragma once

#include "AIObject.h"
#include "../Utility/Clock.h"
#include <vector>

class Actor;

namespace AI
{

	struct Telegram;

	class __declspec(dllexport) AICore
	{
	private:

		//Used for object updates
		bool m_PhysicsOn;

		//Local timer
		Utility::Clock m_Clock;

	public:

		AICore(void);

		~AICore(void);

		/**
		 * Initialize the system
		 */
		bool Initialize(void);

		/*
		 * Shutdown
		 * 
		 * Release all active AI Objects and clear all pointers in containers
		 */

		void Shutdown(void);


		//Update 
		void Update(float dt);

		/**
		 * Setters
		 */

		void SetPhysics(bool physics);

		/**
		 * Getters
		 */
	
		bool GetPhysics(void);

		/**
		 * Console interaction
		 */


		/**
		 * Accessors
		 */

		std::vector<AIObject*>& GetObjects(void);

		AIObject* GetObjectFromId(int id);

		/**
		 * Utilities
		 */

		void RegisterAgent(Actor* owner, AI_TYPE type);

		void ActivateObject(int id);

		void DeactivateObject(int id);

		void ClearAgents(void);
	};


	extern Utility::Clock* AIClock;
}