#include "MessageHandler.h"
#include "AICore.h"
#include "AgentManager.h"

namespace AI
{

	MessageHandler::MessageHandler(void)
	{
		m_Core = nullptr;
	}

	MessageHandler* MessageHandler::Instance()
	{
		static MessageHandler temp;
		return &temp;
	}

	MessageHandler::~MessageHandler(void)
	{
	}

	void MessageHandler::Initialize(AICore* core)
	{
		m_Core = core;
	}

	void MessageHandler::Shutdown(void)
	{
		ClearMessages();
	}

	void MessageHandler::Message(float delay, int sender, int receiver, MESSAGE msg, void* info)
	{
		AIObject* temp = AMI->GetAgentById(receiver);

		if (!temp)
		{
			return;
		}

		Telegram telegram(delay, sender, receiver, msg, info);

		if (delay <= 0.0f)
		{
			Discharge(temp, telegram);
		}

		else
		{
			//Get current time
			float currentTime = AIClock->GetTime();

			//Insert the telegram
			telegram.m_Delivery = currentTime + delay;
		}
	}

	void MessageHandler::Update(float currentTime)
	{
		while ((m_Queue.begin()->m_Delivery < currentTime) &&
			   (m_Queue.begin()->m_Delivery > 0.0))
		{
			//Get the first telegram
			Telegram telegram = *m_Queue.begin();

			//Get the receiver
			AIObject* receiver = AMI->GetAgentById(telegram.m_Receiver);

			//Discharge
			Discharge(receiver, telegram);

			//Pop the telegram
			m_Queue.erase(m_Queue.begin());
		}
	}

	void MessageHandler::Discharge(AIObject* agent, const Telegram& msg)
	{
		if (!agent->HandleMessage(msg))
		{
			//The message was not handled
		}
	}

	void MessageHandler::ClearMessages(void)
	{
		m_Queue.clear();
	}
}