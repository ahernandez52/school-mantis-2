#include "AIObject.h"
#include "Telegram.h"

namespace AI
{


	AIObject::AIObject(void)
	{
		m_Actor = nullptr;
		m_Type = AI_UNDECLARED;
	}

	AIObject::AIObject(Actor* owner) : m_Actor(owner), m_Type(AI_UNDECLARED)
	{
	}

	AIObject::~AIObject(void)
	{
		m_Actor = nullptr;
	}

	/**
	 * Update
	 */

	void AIObject::Update(float dt)
	{
	}

	/**
	 * Messaging
	 */

	bool AIObject::HandleMessage(Telegram m)
	{
		return false;
	}

	/**
	 * Getters
	 */

	Actor* AIObject::GetSelf(void)
	{
		return m_Actor;
	}

	int AIObject::GetId(void)
	{
		return m_Id;
	}

	AI_TYPE AIObject::GetType(void)
	{
		return m_Type;
	}

	bool AIObject::IsActive(void)
	{
		return m_Active;
	}

	Vector3 AIObject::GetPosition(void) const
	{
		return m_Actor->m_Position;
	}

	Vector3 AIObject::GetVelocity(void) const
	{
		return m_Actor->m_Velocity;
	}

	Quaternion AIObject::GetOrientation(void) const
	{
		return m_Actor->m_Orientation;
	}

	/**
	 * Setters
	 */

	void AIObject::SetType(AI_TYPE type)
	{
		m_Type = type;
	}

	void AIObject::SetActive(bool active)
	{
		m_Active = active;
	}

	void AIObject::SetPosition(const Vector3 pos)
	{
		m_Actor->m_Position = pos;
	}

	void AIObject::SetVelocity(const Vector3 vel)
	{
		m_Actor->m_Velocity = vel;
	}

	void AIObject::SetOrientation(const Quaternion q) 
	{
		m_Actor->m_Orientation = q;
	}
}