#include "AICore.h"
#include "AIObject.h"
#include "AgentManager.h"
#include "MessageHandler.h"


namespace AI
{
	Utility::Clock* AIClock = 0;

	AICore::AICore(void)
	{
	}

	AICore::~AICore(void)
	{
	}

	bool AICore::Initialize(void)
	{
		//Reset clock
		m_Clock.ResetClock();

		//Link clock
		AIClock = &m_Clock;

		return true;
	}

	void AICore::Shutdown(void)
	{
		//Shut down agent management
		ClearAgents();
	}

	/**
	 * Update
	 */

	void AICore::Update(float dt)
	{
		//Update internal clock
		m_Clock.Update(dt);

		//Update messages
		Messager->Update(dt);

		//Update agents
		for (auto& agent : AMI->GetAgents())
		{
			if (agent)
			{
				agent->Update(dt);
			}
		}
	}

	/**
	 * Setters
	 */

	void AICore::SetPhysics(bool physics)
	{
		m_PhysicsOn = physics;
	}

	/**
	 * Getters
	 */

	bool AICore::GetPhysics(void)
	{
		return m_PhysicsOn;
	}

	std::vector<AIObject*>& AICore::GetObjects(void)
	{
		return AMI->GetAgents();
	}

	AIObject* AICore::GetObjectFromId(int id)
	{
		return AMI->GetAgentById(id);
	}

	/**
	 * Console interaction
	 */



	/**
	 * Utilities
	 */

	void AICore::RegisterAgent(Actor* owner, AI_TYPE type)
	{
		AMI->RegisterAgent(owner, type);
	}

	void AICore::ActivateObject(int id)
	{
		AMI->GetAgentById(id)->SetActive(true);
	}

	void AICore::DeactivateObject(int id)
	{
		AMI->GetAgentById(id)->SetActive(false);
	}

	void AICore::ClearAgents(void)
	{
		AMI->ClearAgents();
	}
}