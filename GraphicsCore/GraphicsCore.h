#pragma once
#include <d3dx9.h>
#include <d3d9.h>

namespace DX9GraphicsCore
{
	class __declspec(dllexport) GraphicsCore
	{
	private:

	public:
		GraphicsCore();
		~GraphicsCore();

		void Startup(LPDIRECT3DDEVICE9 device);
		void Shutdown();
	};
}